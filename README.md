# Sylvaine Ferrachat 2019-06

Specialized script to plot TC tracks from HURDAT2 (https://www.nhc.noaa.gov/data/#hurdat) or from RSMC (https://www.jma.go.jp/jma/jma-eng/jma-center/rsmc-hp-pub-eg/besttrack.html).


## Installation

```
pip install git+ssh://git@git.iac.ethz.ch/ferrachs/plot-tc-tracks.git
```

## Usage

```
plot_tc  </path/to/tc_data.txt> --outfile my_tc_track_plot.pdf
```

*See `plot_tc --help` for details on all options.*

For a fine-tuned output, it is recommended to store the option values in a configuration file, in order to avoid lengthy commandlines. This file should be named `plot_tc.cfg` and should be located in the directory where `plot_tc` is executed. It will then be automatically read.

It must be a regular txt file and should be formatted the following way:
```
[run]
<option 1> = value
<option 2> = value
...
```

**Remarks:**
* Any line starting with `#` will be ignored.
* All available options in the commandline may be used in the configuration file.
* If an option is defined both in the config file and in the commandline, the command line value will have precedence.
* To disable an entire configuration file, best is to rename or delete it.

Example of a valid configuration file:
```
[run]
sel_storms = GNE 1938, Mitch 1998, Juan 2003
mk_size = 100
legend_ncol = 4
```
See also a longer example in `Goodies/plot_tc.cfg`.
